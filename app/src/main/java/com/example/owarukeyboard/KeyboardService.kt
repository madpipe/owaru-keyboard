package com.example.owarukeyboard

import android.content.Context
import android.inputmethodservice.InputMethodService
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button

class KeyboardService : InputMethodService() {

    private lateinit var keyboardView: View

    // Override the onCreateInputView method to inflate and return your keyboard layout
    override fun onCreateInputView(): View {
        val layoutInflater = layoutInflater

        keyboardView = layoutInflater.inflate(R.layout.keyboard_layout, null)

        setOnClickListeners()

        return keyboardView
    }

    // Override the onStartInput and onFinishInput methods to show and hide the keyboard as needed
    override fun onStartInput(attribute: EditorInfo?, restarting: Boolean) {
        super.onStartInput(attribute, restarting)
        // Show the keyboard
        keyboardView = layoutInflater.inflate(R.layout.keyboard_layout, null)
        keyboardView.visibility = View.VISIBLE
    }

    override fun onFinishInput() {
        super.onFinishInput()
        // Hide the keyboard
        keyboardView = layoutInflater.inflate(R.layout.keyboard_layout, null)
        keyboardView.visibility = View.GONE
    }

    private fun getButtonIDs(): List<Int> {
        val buttonIds = mutableListOf<Int>()

        val rootView = keyboardView as ViewGroup
        val buttonCount = rootView.childCount
        for (i in 0 until buttonCount) {
            val view = rootView.getChildAt(i)
            if (view is Button) {
                buttonIds.add(view.id)
            }
        }
        return buttonIds
    }

    private fun setOnClickListeners() {
        val buttonIds = getButtonIDs()
        for (buttonId in buttonIds) {
            val button = keyboardView.findViewById<Button>(buttonId) ?: continue
            button.setOnClickListener {
                val inputConnection = currentInputConnection
                when (button.id) {
                    R.id.button_enter -> {
                        inputConnection.sendKeyEvent(
                            KeyEvent(
                                KeyEvent.ACTION_DOWN,
                                KeyEvent.KEYCODE_ENTER
                            )
                        )
                    }
                    R.id.button_backspace -> {
                        inputConnection.deleteSurroundingText(1, 0)
                    }
                    R.id.button_left -> {
                        inputConnection.sendKeyEvent(
                            KeyEvent(
                                KeyEvent.ACTION_DOWN,
                                KeyEvent.KEYCODE_DPAD_LEFT
                            )
                        )
                    }
                    R.id.button_right -> {
                        inputConnection.sendKeyEvent(
                            KeyEvent(
                                KeyEvent.ACTION_DOWN,
                                KeyEvent.KEYCODE_DPAD_RIGHT
                            )
                        )
                    }
                    R.id.button_language -> {
                        val inputMethodManager =
                            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        inputMethodManager.showInputMethodPicker()
                    }
                    R.id.button_emoji -> {
                        // TODO
                    }
                    R.id.button_dakuten -> {
                        handleDakuten()
                    }
                    R.id.button_space -> {
                        inputConnection.commitText(" ", 1)
                    }
                    else -> { // Handle a symbol button
                        inputConnection.commitText(button.text, 1)
                    }
                }
            }
        }
    }

    private fun handleDakuten() {
        // Create a map of characters to their dakuten and handakuten versions
        val characterMap = listOf(
            listOf('あ', 'ぁ'),
            listOf('い', 'ぃ'),
            listOf('う', 'ぅ'),
            listOf('え', 'ぇ'),
            listOf('お', 'ぉ'),

            listOf('か', 'が'),
            listOf('き', 'ぎ'),
            listOf('く', 'ぐ'),
            listOf('け', 'げ'),
            listOf('こ', 'ご'),

            listOf('さ', 'ざ'),
            listOf('し', 'じ'),
            listOf('す', 'ず'),
            listOf('せ', 'ぜ'),
            listOf('そ', 'ぞ'),

            listOf('た', 'だ'),
            listOf('ち', 'ぢ'),
            listOf('つ', 'づ'),
            listOf('て', 'で'),
            listOf('と', 'ど'),

            listOf('は', 'ば', 'ぱ'),
            listOf('ひ', 'び', 'ぴ'),
            listOf('ふ', 'ぶ', 'ぷ'),
            listOf('へ', 'べ', 'ぺ'),
            listOf('ほ', 'ぼ', 'ぽ'),

            listOf('や', 'ゃ'),
            listOf('ゆ', 'ゅ'),
            listOf('よ', 'ょ'),
        )
        // Get the current InputConnection
        val inputConnection = currentInputConnection

        // Get the current character from the InputConnection
        val currentChar = inputConnection.getTextBeforeCursor(1, 0)?.get(0)

        val index = characterMap.indexOfFirst { it.contains(currentChar) }
        if (index == -1) {
            return
        }
        // Look up the character in the character map
        val characterList = characterMap[index]

        // Find the index of the current character in the list
        val currentIndex = characterList.indexOf(currentChar)

        // Update the current character with the next one in the list, wrapping around to the beginning if necessary
        val nextIndex = (currentIndex + 1) % characterList.size
        val nextChar = characterList[nextIndex].toString()

        // Replace the current character with the updated one
        inputConnection.deleteSurroundingText(1, 0)
        inputConnection.commitText(nextChar, 1)
    }
}
